/**************************************************************

   Project: Sonic Ecosystem Composition Techniques
      File: SE_Tech_2a__GeneralFeatureExtraction.scd
    Number: 3
 Full Name: Ways of utilizing filtering in Sonic Ecosystems

    Author: Michael Musick
     Email: michael@michaelmusick.com

   Created: 2016-01-27 08:52:06
  Modified: 2016-08-14 08:45:35

   Notes:


**************************************************************/

/*** World Awareness ***/

/*********************************************
The agents of Interactive Music Systems need to
be aware of the environment within which they
will interact with other agents, be they human
or digital.
For most sonic ecosystems, the agent interactions
occur within the physical sonic space of the
room in which they are installed. As these agents
are typically exclusively concerned with sound
interaction, (instead of visual or physical) the
system needs to extract features that describe
the sonic energy of the agents world. This requires
transducers that can convert sonic energy to a digital
representation. This is usually accomplished with
a microphone/s, whose signal is connected to an analog to
digital converter (ADC).
*********************************************/

(
/*********************************************
Perhaps the most basic quality that can be extracted
is the amplitude of an audio signal. This is
accomplished simply by taking the absolute value of
each sample from a digital signal. This results in a
rectified version of the original signal.

In the following example, the amplitude is extracted
and printed on the screen, as well as displayed in
a stethoscope.

NOTE: Close the scope window once you are done.
*********************************************/

SynthDef(\featureExtraction_1_amplitude, {
	arg micIn = 0, out = 0;
	var sig, amplitude;

	sig = SoundIn.ar( micIn );

	// get amplitude of each sample @ Audio Rate (.ar)
	// no attach or release time results in raw amplitude
	amplitude = Amplitude.ar(
		in: sig,
		attackTime: 0,
		releaseTime: 0
	);

	amplitude.poll( label: \amplitude );

	Out.ar(100, amplitude);

}).play;

Stethoscope(s,1, index: 100);

)


(
/*** Envelope Follower ***/
/*********************************************
This example takes the same basic idea as above
to create a smoothed our envelope follower.
This is accomplished by increasing the 60db
convergence time for attacks and decays in the
Amplitude UGen. This resulting signal would be more
appropriate for describing the amplitude shape
of sound events occurring in the space.

In the following example, the envelope follower
and the raw amplitude are both extracted
and displayed in the stethoscope. The envelope
follower is on the top.

NOTE: Close the scope window once you are done.
*********************************************/

SynthDef(\featureExtraction_2_envFollower, {
	arg micIn = 0, out = 0;
	var sig, envFollower, rawAmplitude;

	sig = SoundIn.ar( micIn );

	// get raw amplitude of each sample @ Audio Rate (.ar)
	// no attach or release time results in raw amplitude
	// same as example above
	rawAmplitude = Amplitude.ar(
		in: sig,
		attackTime: 0,
		releaseTime: 0
	);

	// Get filtered amplitude, resulting in envelope following
	envFollower = Amplitude.ar(
		in: sig,
		attackTime: 0.01,
		releaseTime: 0.02
	);

	envFollower.poll( label: \amplitude );

	Out.ar(100, [envFollower, rawAmplitude]);

}).play;

Stethoscope(s,2, index: 100);

)


(
/*** RMS ***/
/*********************************************
RMS or the Root Mean Square of a signal describes
the average power of a signal and will perceptually
translate into how "loud" a signal appears to be.

In this example, the envelope follower is on the
bottom of the stethescope and the RMS is the top
signal.

Notice how the rms signal is smoother then the
envelope follower.

NOTE: Close the scope window once you are done.
*********************************************/

SynthDef(\featureExtraction_3_RMS, {
	arg micIn = 0, out = 0;
	var sig, rawAmplitude, peakAmp, envFollower, rms;
	var fftChain, specPower, peakBin, peakBinVal;

	sig = SoundIn.ar( micIn );

	// Get raw amplitude of each sample @ Control Rate (.kr)
	// No attack or release time results in raw amplitude
	rawAmplitude = Amplitude.kr(
		in: sig,
		attackTime: 0,
		releaseTime: 0
	);
	// track peak amplitude with a moving window of 1""
	peakAmp = Max.kr(rawAmplitude, numsamp: s.sampleRate);


	// Get filtered amplitude, resulting in envelope following
	envFollower = Amplitude.kr(
		in: sig,
		attackTime: 0.01,
		releaseTime: 0.02
	);

	// get RMS
	rms = RunningSum.rms(
		in: sig,
		// how many sample to take the RMS over
		// this is typically a power of 2 value
		numsamp: 2048
	);

	fftChain = FFT( LocalBuf(2048), sig);
	// Get spectral ower value.
	// Must be scaled to be similar to values above
	specPower = FFTPower.kr( fftChain ) * 0.1;
	// Get peak bin power
	#peakBin, peakBinVal = FFTPeak.kr( fftChain );
	peakBinVal = peakBinVal * 0.001;

	Out.kr(100, [rawAmplitude, envFollower, rms, specPower, peakBinVal]);

}).play;
Stethoscope(s,5, index: 100, rate: 'control');
)


(
/*** FFTPower or Spectral Power ***/

/*********************************************
This final amplitude example uses the Spectral Power
UGen. This feature is calculated from a frequency-domain
representation of the incoming audio signal. Hence, this
requires an FFT chain to be passed to the input.

In this example, the spectral power representation
is on the top of the stethescope and RMS is the lower
signal.
*********************************************/

SynthDef(\featureExtraction_4_FFTPower, {
	arg micIn = 0, out = 0;
	var sig, specPower, fftChain, rms;

	sig = SoundIn.ar( micIn );

	// get RMS
	rms = RunningSum.rms(
		in: sig,
		// how many sample to take the RMS over
		// this is typically a power of 2 value
		numsamp: 2048
	);

	// Get filtered amplitude, resulting in envelope following
	fftChain = FFT( LocalBuf(2048), sig);
	specPower = FFTPower.kr( fftChain ) * 0.05;

	specPower.poll( label: \spectral_power );

	Out.kr(100, [specPower, rms]);

}).play;

Stethoscope(s,2, index: 100, rate: 'control');

)

/*********************************************
NOTE: In this document thus far, all signals up to
the last example have been at Audio Rate (.ar).
Typically, signals that describe a sound are
calculated at Control Rate (.kr).
This means for every 1 .kr value that
is calculated there are typically 64 audio rate
samples calculated.
Executing the following command will return
(or set) the block size between audio rate and
control rate signal calculations
s.options.blockSize
The reason for this calculating these signals at
control rate, is that it drastically
saves on processing power and does not effect
sound quality in most situations.
*********************************************/



/*********************************************
As with amplitude, for most qualities of sound that
you would want to use to control your system, there may
by multiple ways or UGens that could be used to accomplish
the task. For example, if a system needs to describe
frequency or pitch of signals in the space there
are many ways to get these values.

Some of these include:
Pitch.kr(), which returns an a fundamental frequency
			estimation of a signal using an
			autocorrelation algorithm.
ZeroCrossing.kr(), this returns another "fundamental
			frequency" estimation from the signal,
			but uses the more simple algorithm for
			zero-crossing. Usually this algorithm
			would be frowned upon when trying to
			accurately estimate the true pitch
			of a signal. However, if the system
			needs low frequency values (0-30 Hz),
			this algorithm may prove more efficient.
			It all depends on the requirements of
			task at hand.
// Chromagram.kr(), this UGen returns an array,
			usually of 12 values, representing the
			12 notes in possible in western music.
			The value for these notes represents the
			amount of energy for each note throughout
			all octaves. This might be useful if
			a system did not need to know the exact
			frequency of signal, but rather, what
			is the strongest note, irregardless of octave.

Examine the Analysis Ugens that are part of SuperCollider
and the sc3-plugins add-on.
Open help (Cmd + d)
Navigate to "Browse"
Then select "UGens->Analysis"


*********************************************/





