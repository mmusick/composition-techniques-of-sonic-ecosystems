/**************************************************************

   Project: Sonic Ecosystem Composition Techniques
      File: SE_Tech_1__Amp_Delay.scd
    Number: 1
 Full Name: Inverse Amplitude and Delay

    Author: Michael Musick
     Email: michael@michaelmusick.com

   Created: 2016-01-27 08:52:06
  Modified: 2016-03-02 08:58:08


   Notes:


**************************************************************/


/*** SIMPLE FEEDBACK ECOSYSTEMS ***/


(
/* Simple Feedback Ecosystem - Limiter Only */

/*********************************************
For this sonic ecosystem, you will need:
- 1 microphone
- a stereo speaker array
These should be in the same room with each other
positioned in a way that would make most sound
engineers nervous.

!!! TURN THE PHYSICAL OUTPUT GAIN ON YOUR SYSTEM
!!! ALL THE WAY DOWN
Turn up the volume slowly after starting the system.

Obviously, this is feedback.
As you turn the volume up, you will hear digital
distortion brought on by the Limiter.ar() UGen.
Examine the Level Meter (Cmd + m), the output signal
does not peak. This should be observable on hardware
interfaces that have level meters as well. Instead,
it stops at -6 dBs, the value we supplied to the Limiter.
*********************************************/

SynthDef(\basicFB_ecosystem, {
	arg micIn = 0, out = 0;
	var sig;

	// INPUT - Get mic signal.
	sig = SoundIn.ar( micIn );

	// OUTPUT - Clean and limit signal. Then send it out.
	// Filter the signal to be in range of audio speaker system
	sig = HPF.ar( LPF.ar( sig, freq: s.sampleRate*0.5), freq: 50);
	// Limit signal so it does not blow up
    sig = Limiter.ar( sig, level: (0.5).neg.dbamp, dur: 0.01 );
    // Filter to remove any DC offset introduced to the signal
    sig = LeakDC.ar( sig );
    // duplicate the signal to stereo and send out
    Out.ar( out, sig.dup(2) );
}).play;
)


(
/* Simple Feedback Ecosystem - Automatic Gain Reduction 1 */

/*********************************************
In this example the RMS of the microphone signal
is inversely mapped to a "Gain Reduction" variable.
This is used to reduce the output signal.
The important thing to note about this is that the
Limiter.ar() now becomes a built in safety that should
not actually be needed.

Again, play with the physical output volume.
At lower levels it should reach a sort of "stasis",
where the feedback is held at a constant level.
If you keep turning up the volume, then you should
start to hear a beating sound as the system "pumps"
the volume. In this situation, the system is reducing
the signal gain more than is necessary to reach stasis.
Therefore it pops backup. Essentially the system is
overshooting in both directions.
*********************************************/

SynthDef(\gainReduction1_ecosystem, {
    arg lagUp = 0.1, lagDown = 0.6, ampMin = 0.1, ampMax = 0.7,
        micIn = 0, out = 0;
    var sig, rms, gainRed;

    // INPUT - get mic signal
    sig = SoundIn.ar( micIn );

    /* INVERSE AMPLITUDE RELATIONSHIP */
    // get the RMS value from the mic signal
    rms = RunningSum.rms( sig, numsamp: 2048 );
    // smooth the RMS signal by 0.1"
	rms = rms.lag( 0.1, 0.6 );
    // map this to gain reduction values
    gainRed = rms.linexp( inMin: 0.1, inMax: 0.7, outMin: 1, outMax: 0.0001 );
    // reduce signal volume based on rms
    sig = sig * gainRed;

    // OUTPUT - Clean and limit signal. Then send it out.
    sig = HPF.ar( LPF.ar( sig, freq: s.sampleRate*0.5), freq: 50);
    sig = Limiter.ar( sig, level: (0.5).neg.dbamp, dur: 0.01 );
    sig = LeakDC.ar( sig );
    Out.ar( out, sig.dup(2) );
}).play;
)


(
/* Simple Feedback Ecosystem - Automatic Gain Reduction 2 */

/*********************************************
In this example, the RMS is taken from the output signal,
after the Limiter.ar(). This has the effect of removing
the overshoot pumping. This is not good or bad, just a
different approach to creating this interconnected system.

This system is notable for the extra headroom it gives the
composition.
*********************************************/

SynthDef(\gainReduction2_ecosystem, {
	arg micIn = 0, out = 0;
	var sig, rms, gainRed;

	// get mic signal
	sig = SoundIn.ar( micIn );

	// feed back the RMS of the output signal
	rms = LocalIn.kr(numChannels:1);
	// map this to gain reduction values
	gainRed = rms.linexp( inMin: 0.1, inMax: 0.3, outMin: 1, outMax: 0.00001 );
	// reduce signal volume by scalar value, based on rms of output signal
	sig = sig * gainRed;

    // OUTPUT - Clean and limit signal.
    sig = HPF.ar( LPF.ar( sig, freq: s.sampleRate*0.5), freq: 50);
    sig = Limiter.ar( sig, level: (0.5).neg.dbamp, dur: 0.01 );
    sig = LeakDC.ar( sig );

	// get the RMS value from the OUTPUT signal
	rms = RunningSum.rms( sig, 2048 );
	// smooth the RMS signal by 1"
	rms = rms.lag(1.0);
	// Send the RMS signal to the top of the signal chain
	LocalOut.kr( rms );

	// create stereo signal
	sig = sig ! 2;
	Out.ar( out, sig );
}).play;
)


(
/* Simple Feedback Ecosystem - Variable Delay Line 1 */

/*********************************************
Simple Delay Line Based Sonic Ecosystem
This system maps the RMS value to delay time of
DelayC.ar() UGen. Feedback can occur when a loop
is created in the audio system. By adjusting the
delay time, the system 'breaks' the loop.

As with previous examples, play with the levels
of the "Main Output Volume" and the Mic Gain.

Depending on the type of relationships you want to
compose, these hardware based controls are how you
go about "tuning" a system for a specific room. By
determining the appropriate hardware based values
for the system, a stable relationship can be
obtained between the physical space and the digital
software. This complete relationship constitutes the
sonic ecosystem.
*********************************************/

SynthDef(\phaseDelay1_ecosystem, {
	arg micIn = 0, out = 0;
	var sig, rms, delayTime;

	// get mic signal
	sig = SoundIn.ar( micIn );

	// get the RMS value from the mic signal
	rms = RunningSum.rms( sig, numsamp: 2048 );
	// smooth the RMS signal by 1.0"
	rms = rms.lag(1);
	delayTime = rms.linlin( inMin: 0.05, inMax: 0.6, outMin: 0, outMax: 0.01 );
	// print the input RMS value and the gain reduction value
	rms.poll( label: \rmsValue );
	delayTime.poll( label: \delayTime );

	// variable delay
	sig = DelayC.ar(
		in: sig,
		maxdelaytime: 0.01,
		delaytime: delayTime
	);

    // OUTPUT - Clean and limit signal.
    sig = HPF.ar( LPF.ar( sig, freq: s.sampleRate*0.5), freq: 50);
    sig = Limiter.ar( sig, level: (3).neg.dbamp, dur: 0.01 );
    sig = LeakDC.ar( sig );
	// create stereo signal
	sig = sig ! 2;
	Out.ar( out, sig );
}).play;
)




(
/* Simple Feedback Ecosystem - Variable Delay Line 2 */

/*********************************************
Simple Delay Line Based Sonic Ecosystem
This is the exact same code as the system above.
The only difference is that DelayC.ar() was replaced
with CombC.ar(). By using a Comb Filter as a variable
delay, the signal is smoothed out during transitions.

Again, by adjusting the mic gain and output volume,
this system can be tuned for the specific room. It is
possible to find a relationship in which slight changes
to the sonic energy of the space (ie. caused by whistling)
can cause the system to be thrown out of stasis, forcing it
to find a stasis relationship.

The other thing that can greatly effect the relationships
of the system is to physically adjust the position of the
microphone and/or speakers (if this is possible).
By changing the position of these sonic transducers,
the relationship of the entire system is adjusted. This will
inevitably result in altered emergent compositions.
*********************************************/

SynthDef(\variableDelay2_ecosystem, {
	arg micIn = 1, out = 0;
	var sig, rms, delayTime;

	// get mic signal
	sig = SoundIn.ar( micIn );

	// get the RMS value from the mic signal
	rms = RunningSum.rms( sig, 2048 );
	// smooth the RMS signal by 0.1"
	rms = rms.lag(0.5);
	delayTime = rms.linlin( 0, 0.6, 0, 0.1 );
	// print the input RMS value and the gain reduction value
	rms.poll( label: \rmsValue );
	delayTime.poll( label: \delayTime );

	// variable comb filter (Comb Delay)
	sig = CombC.ar(
		in: sig,
		maxdelaytime: 0.5,
		delaytime: delayTime,
		decaytime: delayTime * 4
	);

    // OUTPUT - Clean and limit signal.
    sig = HPF.ar( LPF.ar( sig, freq: s.sampleRate*0.5), freq: 50);
    sig = Limiter.ar( sig, level: (3).neg.dbamp, dur: 0.01 );
    sig = LeakDC.ar( sig );
	// create stereo signal
	sig = sig ! 2;
	Out.ar( out, sig );
}).play;
)


(
/*** Variable Delay, Stereo Phase Offset - Sonic Ecosystem  ***/

/*********************************************
This sonic ecosystem builds on the variable delay
systems built above. In this system, the RMS is used
to create a complex relationship between the space
and the delay lines.

Of significance in this composition, there are two delay
lines, one for each speaker. The right speaker delay
line shifts the signal very slightly, which has the effect
of creating phasing artifacts between the two speakers.
This allows the system to exhibit more variability, as
well as moments of stasis.

TODO:
1. Place the microphone near a wall and observe how
this changes the frequencies that setup in the
emergent composition.
2. Walk around the room, get close to the microphone,
come between the microphone and speakers, cup the microphone,
etc. Notice how your physical presence can alter the
relationship of sonic energy in the system. This allows for
participation with the system.
*********************************************/

~der1 = SynthDef(\phaseDelay_sonicEcosystem, {
	arg micIn = 1, out = 2, hiShelfFreq = 4000, hiShelfCut = -12, delayTimeMax = 0.5, maxAmpBoost = 30;
	var sig, sigL, sigR, rms, delayTime, rmsMin, rmsMax, delayMax, rawRMS, ampBoost;

	// get mic signal
	sig = SoundIn.ar( micIn );

	// get the RMS value from the mic signal
	rms = RunningSum.rms( sig, 2048 );
	rawRMS = rms;
	// smooth the RMS signal by 60"
	rms = rms.lag3( 3, 20 ).poll( label: \preRMS);

	//  get the Min RMS for the rmsGate and the delayTime mapping
	rmsMin = rms.lag2(20, 5).clip(0, 0.5).poll( label: \minRMS);
	// get the Max RMS value. Set immediatly, but hold onto for 3 minutes.
	rmsMax = rms.lag(10, 240).poll (label: \maxRMS);

	// adjust the max delayTime,
	// this creates a more harmonically dynamic composition
	delayMax = rmsMax.linexp(0, 0.6, 0.01, delayTimeMax).lag2(15);

	// set the RMS value according to the minRMS.
	// This means RMS is only passed when it is above minRMS
	// This has the effect of stasis in the system.
	rms = Gate.kr( rms, rms - rmsMin ).lag3(20, 50);
	delayTime = rms.linlin(
		rmsMin,
		rmsMax,
		0,
		delayMax
	).clip(0, delayTimeMax);

	// print the input RMS value and the gain reduction value
	rms.poll( label: \rmsValue );
	delayTime.poll( label: \delayTimeL );

	// variable delay filter
	sigL = DelayC.ar(
		in: sig,
		maxdelaytime: delayTimeMax,
		delaytime: delayTime
	);

	// alter phase of Right channel
	// This causes standing waves to break down as RMS changes
	delayTime = delayTime + (
		delayTime.lag3(
			rmsMin.linlin(0,0.5,3,40),
			rmsMin.linlin(0,0.5,60,20))
		* 0.001
	);
	delayTime.poll( label: \delayTimeR );
	sigR = DelayC.ar(
		in: sig,
		maxdelaytime: delayTimeMax,
		delaytime: delayTime
	);

	// combine signals into a single variable
	sig = [sigL, sigR];

	// Boost the amplitude when it is too low
	ampBoost = rawRMS.linlin(0.001, 0.2, maxAmpBoost, 1).clip(1, maxAmpBoost).lag2(10, 6);
	ampBoost.poll( label: \ampBoostFactor );

	sig = sig * ampBoost;


	// equalize the high frequencies
	sig = BHiShelf.ar( sig,
		freq: hiShelfFreq,
		rs: 0.7,
		db: hiShelfCut
	);

	// Limit signal so it does not blow up
	sig = Limiter.ar( sig, level: (-6).dbamp, dur: 0.01 );
	//  Insert high and low pass filters to protect speakers
	sig = LPF.ar( sig, freq: s.sampleRate*0.5 );
	sig = HPF.ar( sig, freq: 60 );

	// start from 0 gain and work up.
	// This prevents initial screeches
	sig = sig * Line.kr(0, 1, 30);
	sig = LeakDC.ar(sig);

	Out.ar( out, sig );
}).play;
)

(
~der2 = SynthDef(\phaseDelay_sonicEcosystem, {
	arg micIn = 0, out = 4, hiShelfFreq = 4000, hiShelfCut = -12, delayTimeMax = 0.5, maxAmpBoost = 30;
	var sig, sigL, sigR, rms, delayTime, rmsMin, rmsMax, delayMax, rawRMS, ampBoost;

	// get mic signal
	sig = SoundIn.ar( micIn );

	// get the RMS value from the mic signal
	rms = RunningSum.rms( sig, 2048 );
	rawRMS = rms;
	// smooth the RMS signal by 60"
	rms = rms.lag3( 3, 20 ).poll( label: \preRMS);

	//  get the Min RMS for the rmsGate and the delayTime mapping
	rmsMin = rms.lag2(20, 5).clip(0, 0.5).poll( label: \minRMS);
	// get the Max RMS value. Set immediatly, but hold onto for 3 minutes.
	rmsMax = rms.lag(10, 240).poll (label: \maxRMS);

	// adjust the max delayTime,
	// this creates a more harmonically dynamic composition
	delayMax = rmsMax.linexp(0, 0.6, 0.01, delayTimeMax).lag2(15);

	// set the RMS value according to the minRMS.
	// This means RMS is only passed when it is above minRMS
	// This has the effect of stasis in the system.
	rms = Gate.kr( rms, rms - rmsMin ).lag3(20, 50);
	delayTime = rms.linlin(
		rmsMin,
		rmsMax,
		0,
		delayMax
	).clip(0, delayTimeMax);

	// print the input RMS value and the gain reduction value
	rms.poll( label: \rmsValue );
	delayTime.poll( label: \delayTimeL );

	// variable comb filter (Comb Delay)
	sigL = DelayC.ar(
		in: sig,
		maxdelaytime: delayTimeMax,
		delaytime: delayTime
	);

	// alter phase of Right channel
	// This causes standing waves to break down as RMS changes
	delayTime = delayTime + (
		delayTime.lag3(
			rmsMin.linlin(0,0.5,3,40),
			rmsMin.linlin(0,0.5,60,20))
		* 0.001
	);
	delayTime.poll( label: \delayTimeR );
	sigR = DelayC.ar(
		in: sig,
		maxdelaytime: delayTimeMax,
		delaytime: delayTime
	);

	// combine signals into a single variable
	sig = [sigL, sigR];

	// Boost the amplitude when it is too low
	ampBoost = rawRMS.linlin(0.001, 0.2, maxAmpBoost, 1).clip(1, maxAmpBoost).lag2(10, 6);
	ampBoost.poll( label: \ampBoostFactor );

	sig = sig * ampBoost;


	// equalize the high frequencies
	sig = BHiShelf.ar( sig,
		freq: hiShelfFreq,
		rs: 0.7,
		db: hiShelfCut
	);

	// Limit signal so it does not blow up
	sig = Limiter.ar( sig, level: (-6).dbamp, dur: 0.01 );
	//  Insert high and low pass filters to protect speakers
	sig = LPF.ar( sig, freq: s.sampleRate*0.5 );
	sig = HPF.ar( sig, freq: 60 );

	// start from 0 gain and work up.
	// This prevents initial screeches
	sig = sig * Line.kr(0, 1, 30);
	sig = LeakDC.ar(sig);

	Out.ar( out, sig );
}).play;
)

~der1.free;
~der2.free;