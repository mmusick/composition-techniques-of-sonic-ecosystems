Composition Techniques of Feedback-Based Sonic Ecosystems
==========================================================
As emerging from work in the Sonic Spaces Project
----------------------------------------------------------
For more information about the Sonic Spaces Project, please visit: [michaelmusick.com](http://michaelmusick.com/sonic_spaces_project).

## Information About These Files ##
The files contained in this repository should be considered as examples, and possible techniques that one could use to compose a feedback-based sonic ecosystem composition.

As research in this project progresses, this repository will be updated to include more examples, code, techniques, and research outcomes. 

## Basic Definitions ##

### Sonic Ecosystem ###
Sonic Ecosystems, are a sub-discipline of interactive music systems, which attempt to represent, adapt, and explore principles of other systems through an open sonic interface. These systems create a complex network of interconnected agents, based on artistically inspired relationships and principles from real-world ecosystems. The characteristics of these compositions create open-form, spatio-temporal, interactive music systems. This model creates flexibility in the presentational and participatory aspects of the systems, as well as creating engaging interactive opportunities that touch on notions of computer agency, experience-based art, and site-specific couplings. 

The practice of composing Sonic Ecosystems is defined by a number of characteristics. The first, which I consider as a strict requirement, is that the majority, if not all, of the interaction between agents of the system (human and/or software) exists in the open sonic space of the room. This means that the sonic space of the chamber in which the system is installed serves as the interface for information exchange, with microphones placed throughout the space. 

The emergent musical properties of the system should be capable of creating a dynamic and diverse range of soundscapes, able to move between states of stability or stasis and states of volatility as the system works towards reestablishing equilibrium. 

Ecosystems imply some source of energy exchange and energy use between agents of the system. Typically, for the system to maintain stasis, this energy needs to be balanced throughout, and the influx of new energy controlled. Since the interface for a sonic ecosystem is the sonic space, this implies that the major form of energy for agents is in the form of sonic energy. 

Finally, as this is intended as an interactive art form it is important for participants to experience these systems. Participation should provide opportunities to reconsider one’s relationship to other systems through experience-based play. Software agents also need to be capable of collaborating and engaging with the human participants in the system. In order for this to occur, the sonic ecosystem must be capable of handling unknown types of sonic energy by human-agents. This sonic energy should stimulate and potentially direct the emergent music of the system.

### Interactive Music System ###
A music system where every participating agent (software-agent or human-agent) has an equal opportunity to contribute information and thereby affect the state of one or more collaborating agents. With regards to human-participants, this should allow for self-directed construction of meaning relative and pertinent to the participants own lived experience. Furthermore, the primary goal for the system is for musicking or the act of creating, listening, or considering music. 

### Music ###

The perception of organized sound as meaningful. This requires active perception towards the sonic domain by the listener. It also implies that the listener finds the organized sound engaging enough so as to allow the potential for affectual change to occur as a result. This does not necessarily require a creator of the sound, but only requires a listener.

### Music Technology ###

The systematic application of knowledge to evolve the means of creation and relationships of individuals to any act of musicking.

### Agent ###

A component of a system that exhibits a crucial role in the decision-making process or actions of the system. At the top-level of the sonic ecosystem this includes software-agents and human-agents. A software-agent may be itself, a multi-agent system, with individual agent-modules, responsible for singular tasks, sharing their information internally with the other agent-modules of the software-agent. This creates a hierarchical relationship of module-agents to the software-agent, which itself is just one part of the full multi-agent, interconnected ecosystem.

### Participant ###

A human-agent who is physically present in the space of the system. This person may contribute music or sonic energy to the system, or simply listen. Regardless, their presence in the room will affect the acoustic properties of the space in slight ways, and therefore they become a part of the system.

### Sonic Energy ###

The audible sound/music occurring in the installation space. Sonic energy is the sound or music in the space that can be acquired through microphones and converted to a digital representation for analysis by the digital system. This sonic energy is analyzed and described within the system in terms of its acoustic properties. 

### Open Sonic Interface ###

This implies that the entire room, chamber, or space that the system is installed in becomes the interface for data exchange between agents. Human-agents are naturally able to do this by being in the space and hearing the total music emerging. Software-agent are afforded this capability by placing a microphone or array of microphones around the space so that the entire emergent music can be heard. This is opposed to the more common direct-mic’ing technique that would try to capture or encourage participants to focus on making music directly into a microphone.

### Research-Led Practice ###

Practice that is directed by research findings. Typically occurs in a cyclical, iterative process with practice-led research.

### Practice-Led Research ###

Research that is conducted through practice. This allows the researcher to create knowledge through the critical study of their art or craft. This research may occur as part of the artistic practice, or occur separately but take its impetus from artistic practice.


